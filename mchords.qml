import QtQuick 2.1
import MuseScore 3.0

MuseScore {
    version:  "3.0"
    description: "Create chord notes."
    menuPath: "Plugins.Chords"
    requiresScore: true

    onRun: {
        var teoria;

        function findChord(cursor) {
            if (!cursor.segment) return null;
            var anns = cursor.segment.annotations
            for (var a in anns) {
                var ann = anns[a];
                if (Math.floor(ann.track/4) == cursor.staffIdx && ann.type == Element.HARMONY) {
                    try {
                        return teoria.chord(ann.text);
                    } catch(e) {
                        console.warn('Cannot parse chord:', ann.text);
                    }
                }
            }
            return null;
        }

        function tpcToNote(tpc) {
            tpc += 1;
            return 'FCGDAEB'[tpc%7] + ['bb','b','','#','X'][Math.floor(tpc/7)];
        }

        ////////////////////////////////////////
        /// teoria /////////////////////////////
        ////////////////////////////////////////
        !function(t){var n={};function r(e){if(n[e])return n[e].exports;var i=n[e]={i:e,l:!1,exports:{}};return t[e].call(i.exports,i,i.exports,r),i.l=!0,i.exports}r.m=t,r.c=n,r.d=function(t,n,e){r.o(t,n)||Object.defineProperty(t,n,{enumerable:!0,get:e})},r.r=function(t){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(t,"__esModule",{value:!0})},r.t=function(t,n){if(1&n&&(t=r(t)),8&n)return t;if(4&n&&"object"==typeof t&&t&&t.__esModule)return t;var e=Object.create(null);if(r.r(e),Object.defineProperty(e,"default",{enumerable:!0,value:t}),2&n&&"string"!=typeof t)for(var i in t)r.d(e,i,function(n){return t[n]}.bind(null,i));return e},r.n=function(t){var n=t&&t.__esModule?function(){return t.default}:function(){return t};return r.d(n,"a",n),n},r.o=function(t,n){return Object.prototype.hasOwnProperty.call(t,n)},r.p="",r(r.s=6)}([function(t,n){t.exports={notes:{c:[0,0],d:[-1,2],e:[-2,4],f:[1,-1],g:[0,1],a:[-1,3],b:[-2,5],h:[-2,5]},intervals:{unison:[0,0],second:[3,-5],third:[2,-3],fourth:[1,-1],fifth:[0,1],sixth:[3,-4],seventh:[2,-2],octave:[1,0]},intervalFromFifth:["second","sixth","third","seventh","fourth","unison","fifth"],intervalsIndex:["unison","second","third","fourth","fifth","sixth","seventh","octave","ninth","tenth","eleventh","twelfth","thirteenth","fourteenth","fifteenth"],fifths:["f","c","g","d","a","e","b"],accidentals:["bb","b","","#","x"],sharp:[-4,7],A4:[3,3],durations:{.25:"longa",.5:"breve",1:"whole",2:"half",4:"quarter",8:"eighth",16:"sixteenth",32:"thirty-second",64:"sixty-fourth",128:"hundred-twenty-eighth"},qualityLong:{P:"perfect",M:"major",m:"minor",A:"augmented",AA:"doubly augmented",d:"diminished",dd:"doubly diminished"},alterations:{perfect:["dd","d","P","A","AA"],minor:["dd","d","m","M","A","AA"]},symbols:{min:["m3","P5"],m:["m3","P5"],"-":["m3","P5"],M:["M3","P5"],"":["M3","P5"],"+":["M3","A5"],aug:["M3","A5"],dim:["m3","d5"],o:["m3","d5"],maj:["M3","P5","M7"],dom:["M3","P5","m7"],"ø":["m3","d5","m7"],5:["P5"]},chordShort:{major:"M",minor:"m",augmented:"aug",diminished:"dim","half-diminished":"7b5",power:"5",dominant:"7"},stepNumber:{unison:1,first:1,second:2,third:3,fourth:4,fifth:5,sixth:6,seventh:7,octave:8,ninth:9,eleventh:11,thirteenth:13},intervalSolfege:{dd1:"daw",d1:"de",P1:"do",A1:"di",AA1:"dai",d2:"raw",m2:"ra",M2:"re",A2:"ri",AA2:"rai",d3:"maw",m3:"me",M3:"mi",A3:"mai",dd4:"faw",d4:"fe",P4:"fa",A4:"fi",AA4:"fai",dd5:"saw",d5:"se",P5:"so",A5:"si",AA5:"sai",d6:"law",m6:"le",M6:"la",A6:"li",AA6:"lai",d7:"taw",m7:"te",M7:"ti",A7:"tai",dd8:"daw",d8:"de",P8:"do",A8:"di",AA8:"dai"}}},function(t,n,r){var e=r(0),i=r(5),o=r(11);function a(t){if(!(this instanceof a))return new a(t);this.coord=t}a.prototype={name:function(){return e.intervalsIndex[this.number()-1]},semitones:function(){return i.sum(i.mul(this.coord,[12,7]))},number:function(){return Math.abs(this.value())},value:function(){var t=Math.floor((this.coord[1]-2)/7)+1,n=i.mul(e.sharp,t),r=i.sub(this.coord,n),o=e.intervalFromFifth[r[1]+5],a=r[0]-e.intervals[o][0],s=e.stepNumber[o]+7*a;return s>0?s:s-2},type:function(){return e.intervals[this.base()][0]<=1?"perfect":"minor"},base:function(){var t=i.mul(e.sharp,this.qualityValue()),n=i.sub(this.coord,t)[1];n=(n=this.value()>0?n+5:-(n-5)%7)<0?e.intervalFromFifth.length+n:n;var r=e.intervalFromFifth[n];return"unison"===r&&this.number()>=8&&(r="octave"),r},direction:function(t){return t?((this.value()>=1?"up":"down")!==t&&(this.coord=i.mul(this.coord,-1)),this):this.value()>=1?"up":"down"},simple:function(t){var n=e.intervals[this.base()],r=i.mul(e.sharp,this.qualityValue());return n=i.add(n,r),t||(n="down"===this.direction()?i.mul(n,-1):n),new a(n)},isCompound:function(){return this.number()>8},octaves:function(){var t,n;return"up"===this.direction()?(t=i.mul(e.sharp,this.qualityValue()),n=i.sub(this.coord,t)[0]-e.intervals[this.base()][0]):(t=i.mul(e.sharp,-this.qualityValue()),n=-(i.sub(this.coord,t)[0]+e.intervals[this.base()][0])),n},invert:function(){var t=this.base(),n=this.qualityValue(),r="minor"===this.type()?-(n-1):-n,o=9-e.stepNumber[t]-1,s=e.intervals[e.intervalsIndex[o]];return new a(s=i.add(s,i.mul(e.sharp,r)))},quality:function(t){var n=e.alterations[this.type()][this.qualityValue()+2];return t?e.qualityLong[n]:n},qualityValue:function(){return"down"===this.direction()?Math.floor((-this.coord[1]-2)/7)+1:Math.floor((this.coord[1]-2)/7)+1},equal:function(t){return this.coord[0]===t.coord[0]&&this.coord[1]===t.coord[1]},greater:function(t){var n=this.semitones(),r=t.semitones();return n===r?this.number()>t.number():n>r},smaller:function(t){return!this.equal(t)&&!this.greater(t)},add:function(t){return new a(i.add(this.coord,t.coord))},toString:function(t){var n=t?this.number():this.value();return this.quality()+n}},a.toCoord=function(t){var n=o(t);if(!n)throw new Error("Invalid simple format interval");return new a(n)},a.from=function(t,n){return t.interval(n)},a.between=function(t,n){return new a(i.sub(n.coord,t.coord))},a.invert=function(t){return a.toCoord(t).invert().toString()},t.exports=a},function(t,n,r){var e=r(8),i=r(9),o=r(10),a=r(0),s=r(5),u=r(1);function c(t,n,r){for(;r>0;r--)t+=n;return t}function h(t,n){if(!(this instanceof h))return new h(t,n);n=n||{},this.duration={value:n.value||4,dots:n.dots||0},this.coord=t}h.prototype={octave:function(){return this.coord[0]+a.A4[0]-a.notes[this.name()][0]+4*this.accidentalValue()},name:function(){var t=this.accidentalValue(),n=this.coord[1]+a.A4[1]-7*t+1;return a.fifths[n]},accidentalValue:function(){return Math.round((this.coord[1]+a.A4[1]-2)/7)},accidental:function(){return a.accidentals[this.accidentalValue()+2]},key:function(t){return t?7*this.coord[0]+4*this.coord[1]+29:12*this.coord[0]+7*this.coord[1]+49},midi:function(){return this.key()+20},fq:function(t){return o(this.coord,t)},chroma:function(){var t=(s.sum(s.mul(this.coord,[12,7]))-3)%12;return t<0?t+12:t},interval:function(t){return"string"==typeof t&&(t=u.toCoord(t)),t instanceof u?new h(s.add(this.coord,t.coord),this.duration):t instanceof h?new u(s.sub(t.coord,this.coord)):void 0},transpose:function(t){return this.coord=s.add(this.coord,t.coord),this},helmholtz:function(){var t=this.octave(),n=this.name(),r=t<3?",":"'",e=t<2?2-t:t-3;return c((n=t<3?n.toUpperCase():n.toLowerCase())+this.accidental(),r,e)},scientific:function(){return this.name().toUpperCase()+this.accidental()+this.octave()},enharmonics:function(t){var n=this.key(),r=t?2:3;return["m3","m2","m-2","m-3"].map(this.interval.bind(this)).filter((function(t){var e=t.accidentalValue(),i=n-(t.key()-e);if(i<r&&i>-r){var o=s.mul(a.sharp,i-e);return t.coord=s.add(t.coord,o),!0}}))},solfege:function(t,n){var r,e,i,o=t.tonic.interval(this);return"down"===o.direction()&&(o=o.invert()),n&&(e=(i=(i=(this.key(!0)-t.tonic.key(!0))/7)>=0?Math.floor(i):-Math.ceil(-i))>=0?"'":","),r=a.intervalSolfege[o.simple(!0).toString()],n?c(r,e,Math.abs(i)):r},scaleDegree:function(t){var n=t.tonic.interval(this);return("down"===n.direction()||0===n.coord[1]&&0!==n.coord[0])&&(n=n.invert()),n=n.simple(!0).coord,t.scale.reduce((function(t,r,e){var i=u.toCoord(r).coord;return i[0]===n[0]&&i[1]===n[1]?e+1:t}),0)},durationName:function(){return a.durations[this.duration.value]},durationInSeconds:function(t,n){var r=60/t/(this.duration.value/4)/(n/4);return 2*r-r/Math.pow(2,this.duration.dots)},toString:function(t){return this.name()+this.accidental()+(t?"":this.octave())}},h.fromString=function(t,n){var r=e(t);return r||(r=i(t)),new h(r,n)},h.fromKey=function(t){var n=Math.floor((t-4)/12),r=t-12*n-4,e=a.fifths[(2*Math.round(r/2)+1)%7],i=s.sub(a.notes[e],a.A4),o=s.add(i,[n+1,0]),u=t-49-s.sum(s.mul(o,[12,7]));return new h(u?s.add(o,s.mul(a.sharp,u)):o)},h.fromFrequency=function(t,n){var r,e,i;return n=n||440,r=49+(Math.log(t)-Math.log(n))/Math.log(2)*12,r=Math.round(r),i=n*Math.pow(2,(r-49)/12),e=Math.log(t/i)/Math.log(2)*1200,{note:h.fromKey(r),cents:e}},h.fromMIDI=function(t){return h.fromKey(t-20)},t.exports=h},function(t,n){var r={c:[0,0],d:[-1,2],e:[-2,4],f:[1,-1],g:[0,1],a:[-1,3],b:[-2,5],h:[-2,5]};t.exports=function(t){return t in r?[r[t][0],r[t][1]]:null},t.exports.notes=r,t.exports.A4=[3,3],t.exports.sharp=[-4,7]},function(t,n){var r={bb:-2,b:-1,"":0,"#":1,x:2};t.exports=function(t){return r[t]},t.exports.interval=function(t){var n=r[t];return[-4*n,7*n]}},function(t,n){t.exports={add:function(t,n){return[t[0]+n[0],t[1]+n[1]]},sub:function(t,n){return[t[0]-n[0],t[1]-n[1]]},mul:function(t,n){return"number"==typeof n?[t[0]*n,t[1]*n]:[t[0]*n[0],t[1]*n[1]]},sum:function(t){return t[0]+t[1]}}},function(t,n,r){teoria=r(7)},function(t,n,r){var e,i=r(2),o=r(1),a=r(12),s=r(14);function u(t,n){if("string"==typeof t)return o.toCoord(t);if("string"==typeof n&&t instanceof i)return o.from(t,o.toCoord(n));if(n instanceof o&&t instanceof i)return o.from(t,n);if(n instanceof i&&t instanceof i)return o.between(t,n);throw new Error("Invalid parameters")}function c(t,n){return"string"==typeof t?i.fromString(t,n):new i(t,n)}u.toCoord=o.toCoord,u.from=o.from,u.between=o.between,u.invert=o.invert,c.fromString=i.fromString,c.fromKey=i.fromKey,c.fromFrequency=i.fromFrequency,c.fromMIDI=i.fromMIDI,e={note:c,chord:function(t,n){var r,e;if("string"==typeof t){if((r=t.match(/^([a-h])(x|#|bb|b?)/i))&&r[0])return e="number"==typeof n?n.toString(10):"4",new a(i.fromString(r[0].toLowerCase()+e),t.substr(r[0].length))}else if(t instanceof i)return new a(t,n);throw new Error("Invalid Chord. Couldn't find note name")},interval:u,scale:function(t,n){return t=t instanceof i?t:e.note(t),new s(t,n)},Note:i,Chord:a,Scale:s,Interval:o},r(15)(e),t.exports=e},function(t,n,r){var e=r(3),i=r(4);t.exports=function(t){var n=t.match(/^([a-h])(x|#|bb|b?)(-?\d*)/i);if(n&&t===n[0]&&n[3].length){var r=n[1],o=+n[3],a=n[2].length?n[2].toLowerCase():"",s=i.interval(a),u=e(r.toLowerCase());return u[0]+=o,u[0]+=s[0]-e.A4[0],u[1]+=s[1]-e.A4[1],u}}},function(t,n,r){var e=r(3),i=r(4);t.exports=function(t){var n=(t=t.replace(/\u2032/g,"'").replace(/\u0375/g,",")).match(/^(,*)([a-h])(x|#|bb|b?)([,\']*)$/i);if(!n||t!==n[0])throw new Error("Invalid formatting");var r,o=n[2],a=n[1],s=n[4],u=o===o.toLowerCase();if(a){if(u)throw new Error("Invalid formatting - found commas before lowercase note");r=2-a.length}else if(s)if(s.match(/^'+$/)&&u)r=3+s.length;else{if(!s.match(/^,+$/)||u)throw new Error("Invalid formatting - mismatch between octave indicator and letter case");r=2-s.length}else r=u?3:2;var c=i.interval(n[3].toLowerCase()),h=e(o.toLowerCase());return h[0]+=r,h[0]+=c[0]-e.A4[0],h[1]+=c[1]-e.A4[1],h}},function(t,n){t.exports=function(t,n){return"number"==typeof t?(n=t,function(t){return n*Math.pow(2,(12*t[0]+7*t[1])/12)}):(n=n||440)*Math.pow(2,(12*t[0]+7*t[1])/12)}},function(t,n){var r=/^(AA|A|P|M|m|d|dd)(-?\d+)$/,e=[-4,7],i=["dd","d","P","A","AA"],o=["dd","d","m","M","A","AA"],a=[[0,0],[3,-5],[2,-3],[1,-1],[0,1],[3,-4],[2,-2],[1,0]];t.exports=function(t){var n=t.match(r);if(!n)return null;var s=n[1],u=+n[2],c=u<0?-1:1,h=(u=c<0?-u:u)>8?u%7||7:u,f=(u-h)/7,l=a[h-1],d=(l[0]<=1?i:o).indexOf(s)-2;return-3===d?null:[c*(l[0]+f+e[0]*d),c*(l[1]+e[1]*d)]},t.exports.coords=a.slice(0)},function(t,n,r){var e=r(13),i=r(0),o=r(2),a=r(1);function s(t,n){if(!(this instanceof s))return new s(t,n);n=n||"",this.name=t.name().toUpperCase()+t.accidental()+n,this.symbol=n,this.root=t,this.intervals=[],this._voicing=[];var r=n.split("/");if(2===r.length&&"9"!==r[1].trim()?(n=r[0],r=r[1].trim()):r=null,this.intervals=e(n).map(a.toCoord),this._voicing=this.intervals.slice(),r){var i,u,c=this.intervals;u=o.fromString(r+(t.octave()+1)),r=(i=a.between(t,u)).simple(),i=i.invert().direction("down"),this._voicing=[i];for(var h=0,f=c.length;h<f;h++)c[h].simple().equal(r)||this._voicing.push(c[h])}}s.prototype={notes:function(){var t=this.root;return this.voicing().map((function(n){return t.interval(n)}))},simple:function(){return this.notes().map((function(t){return t.toString(!0)}))},bass:function(){return this.root.interval(this._voicing[0])},voicing:function(t){if(!t)return this._voicing;this._voicing=[];for(var n=0,r=t.length;n<r;n++)this._voicing[n]=a.toCoord(t[n]);return this},resetVoicing:function(){this._voicing=this.intervals},dominant:function(t){return t=t||"",new s(this.root.interval("P5"),t)},subdominant:function(t){return t=t||"",new s(this.root.interval("P4"),t)},parallel:function(t){t=t||"";var n=this.quality();if("triad"!==this.chordType()||"diminished"===n||"augmented"===n)throw new Error("Only major/minor triads have parallel chords");return"major"===n?new s(this.root.interval("m3","down"),"m"):new s(this.root.interval("m3","up"))},quality:function(){for(var t,n,r,e=this.intervals,i=0,o=e.length;i<o;i++)3===e[i].number()?t=e[i]:5===e[i].number()?n=e[i]:7===e[i].number()&&(r=e[i]);if(t)return t=(t="down"===t.direction()?t.invert():t).simple().toString(),n&&(n=(n="down"===n.direction?n.invert():n).simple().toString()),r&&(r=(r="down"===r.direction?r.invert():r).simple().toString()),"M3"===t?"A5"===n?"augmented":"P5"===n&&"m7"===r?"dominant":"major":"m3"===t?"P5"===n?"minor":"d5"===n?"m7"===r?"half-diminished":"diminished":"minor":void 0},chordType:function(){var t,n,r,e,i,o=this.intervals.length;if(2===o)return"dyad";if(3===o){for(n={unison:!1,third:!1,fifth:!1},e=0;e<o;e++)r=(t=this.intervals[e]).invert(),t.base()in n?n[t.base()]=!0:r.base()in n&&(n[r.base()]=!0);i=n.unison&&n.third&&n.fifth?"triad":"trichord"}else if(4===o){for(n={unison:!1,third:!1,fifth:!1,seventh:!1},e=0;e<o;e++)r=(t=this.intervals[e]).invert(),t.base()in n?n[t.base()]=!0:r.base()in n&&(n[r.base()]=!0);n.unison&&n.third&&n.fifth&&n.seventh&&(i="tetrad")}return i||"unknown"},get:function(t){if("string"==typeof t&&t in i.stepNumber){var n,r,e=this.intervals;for(t=i.stepNumber[t],n=0,r=e.length;n<r;n++)if(e[n].number()===t)return this.root.interval(e[n]);return null}throw new Error("Invalid interval name")},interval:function(t){return new s(this.root.interval(t),this.symbol)},transpose:function(t){return this.root.transpose(t),this.name=this.root.name().toUpperCase()+this.root.accidental()+this.symbol,this},toString:function(){return this.name}},t.exports=s},function(t,n){var r={m:["m3","P5"],mi:["m3","P5"],min:["m3","P5"],"-":["m3","P5"],M:["M3","P5"],ma:["M3","P5"],"":["M3","P5"],"+":["M3","A5"],aug:["M3","A5"],dim:["m3","d5"],o:["m3","d5"],maj:["M3","P5","M7"],dom:["M3","P5","m7"],"ø":["m3","d5","m7"],5:["P5"],"6/9":["M3","P5","M6","M9"]};t.exports=function(t){var n,e,i="quality",o=[],a=2,s=["P1","M3","P5","m7","M9","P11","M13"],u=!1;function c(t){for(var n=r[t],e=0,i=n.length;e<i;e++)s[e+1]=n[e];a=n.length}for(var h=0,f=(t=t.replace(/[,\s\(\)]/g,"")).length;h<f;h++){if(!(n=t[h]))return;if("quality"===i){var l=h+2<f?t.substr(h,3).toLowerCase():null,d=h+1<f?t.substr(h,2).toLowerCase():null;(e=l in r?l:d in r?d:n in r?n:"")&&c(e),"M"!==e&&"ma"!==e&&"maj"!==e||(u=!0),h+=e.length-1,i="extension"}else if("extension"===i){if(n="1"===n&&t[h+1]?+t.substr(h,2):+n,isNaN(n)||6===n)6===n?(s[3]="M6",a=Math.max(3,a)):h-=1;else{if((a=(n-1)/2)!==Math.round(a))return new Error("Invalid interval extension: "+n.toString(10));"o"===e||"dim"===e?s[3]="d7":u&&(s[3]="M7"),h+=n>=10?1:0}i="alterations"}else if("alterations"===i){var m=t.substr(h).split(/(#|b|add|maj|sus|M)/i),v=!1,p=!1;if(1===m.length)return new Error("Invalid alteration");if(0!==m[0].length)return new Error("Invalid token: '"+m[0]+"'");var g=!1;m.forEach((function(t,n,r){if(g||!t.length)return g=!1;var e=r[n+1],i=t.toLowerCase();if("M"===t||"maj"===i)"7"===e&&(g=!0),a=Math.max(3,a),s[3]="M7";else if("sus"===i){var u="P4";"2"!==e&&"4"!==e||(g=!0,"2"===e&&(u="M2")),s[1]=u}else if("add"===i)"9"===e?o.push("M9"):"11"===e?o.push("P11"):"13"===e&&o.push("M13"),g=!0;else if("b"===i)v=!0;else if("#"===i)p=!0;else{var c,h,f=+t;if(isNaN(f)||String(f).length!==t.length)return new Error("Invalid token: '"+t+"'");if(6===f)return s[3]=p?"A6":v?"m6":"M6",void(a=Math.max(3,a));if(a<(h=(f-1)/2)&&(a=h),f<5||7===f||h!==Math.round(h))return new Error("Invalid interval alteration: "+f);c=s[h][0],p?"d"===c?c="m":"m"===c?c="M":"M"!==c&&"P"!==c||(c="A"):v&&("A"===c?c="M":"M"===c?c="m":"m"!==c&&"P"!==c||(c="d")),p=v=!1,s[h]=c+f}})),i="ended"}else if("ended"===i)break}return s.slice(0,a+1).concat(o)}},function(t,n,r){var e=r(0),i=r(1),o={aeolian:["P1","M2","m3","P4","P5","m6","m7"],blues:["P1","m3","P4","d5","P5","m7"],chromatic:["P1","m2","M2","m3","M3","P4","A4","P5","m6","M6","m7","M7"],dorian:["P1","M2","m3","P4","P5","M6","m7"],doubleharmonic:["P1","m2","M3","P4","P5","m6","M7"],harmonicminor:["P1","M2","m3","P4","P5","m6","M7"],ionian:["P1","M2","M3","P4","P5","M6","M7"],locrian:["P1","m2","m3","P4","d5","m6","m7"],lydian:["P1","M2","M3","A4","P5","M6","M7"],majorpentatonic:["P1","M2","M3","P5","M6"],melodicminor:["P1","M2","m3","P4","P5","M6","M7"],minorpentatonic:["P1","m3","P4","P5","m7"],mixolydian:["P1","M2","M3","P4","P5","M6","m7"],phrygian:["P1","m2","m3","P4","P5","m6","m7"],wholetone:["P1","M2","M3","A4","A5","A6"]};function a(t,n){if(!(this instanceof a))return new a(t,n);var r,e;if(!("coord"in t))throw new Error("Invalid Tonic");if("string"==typeof n){if(r=n,!(n=o[n]))throw new Error("Invalid Scale")}else for(e in o)if(o.hasOwnProperty(e)&&o[e].toString()===n.toString()){r=e;break}this.name=r,this.tonic=t,this.scale=n}o.harmonicchromatic=o.chromatic,o.minor=o.aeolian,o.major=o.ionian,o.flamenco=o.doubleharmonic,a.prototype={notes:function(){for(var t=[],n=0,r=this.scale.length;n<r;n++)t.push(this.tonic.interval(this.scale[n]));return t},simple:function(){return this.notes().map((function(t){return t.toString(!0)}))},type:function(){var t=this.scale.length-2;if(t<8)return["di","tri","tetra","penta","hexa","hepta","octa"][t]+"tonic"},get:function(t){t="string"==typeof t&&t in e.stepNumber?e.stepNumber[t]:t;var n,r,o=this.scale.length;return t<0?(n=this.scale[t%o+o-1],r=Math.floor((t-1)/o)):t%o==0?(n=this.scale[o-1],r=t/o-1):(n=this.scale[t%o-1],r=Math.floor(t/o)),this.tonic.interval(n).interval(new i([r,0]))},solfege:function(t,n){return t?this.get(t).solfege(this,n):this.notes().map((function(t){return t.solfege(this,n)}))},interval:function(t){return t="string"==typeof t?i.toCoord(t):t,new a(this.tonic.interval(t),this.scale)},transpose:function(t){var n=this.interval(t);return this.scale=n.scale,this.tonic=n.tonic,this}},a.KNOWN_SCALES=Object.keys(o),t.exports=a},function(t,n,r){var e=r(0);t.exports=function(t){var n=t.Note,r=t.Chord,i=t.Scale;n.prototype.chord=function(t){return t=t in e.chordShort?e.chordShort[t]:t,new r(this,t)},n.prototype.scale=function(t){return new i(this,t)}}}]);
        ////////////////////////////////////////

        curScore.startCmd();

        var cursor = curScore.newCursor();
        cursor.rewind(1);
        var startStaff;
        var endStaff;
        var endTick;
        var fullScore = false;
        if (!cursor.segment) { // no selection
          fullScore = true;
          startStaff = 0; // start with 1st staff
          endStaff = curScore.nstaves - 1; // and end with last
        } else {
            startStaff = cursor.staffIdx;
            cursor.rewind(2);
            if (cursor.tick === 0) {
                // this happens when the selection includes
                // the last measure of the score.
                // rewind(2) goes behind the last segment (where
                // there's none) and sets tick=0
                endTick = curScore.lastSegment.tick + 1;
            } else {
                endTick = cursor.tick;
            }
            endStaff = cursor.staffIdx;
        }

        if (fullScore) {
            console.log('Applying to full score');
        } else {
            console.log('Applying to selection only');
        }

        for (var staff = startStaff; staff <= endStaff; staff++) {
            var transposition = null;

            for (var voice = 0; voice < 4; voice++) {
                cursor.rewind(1); // sets voice to 0
                cursor.voice = voice;
                cursor.staffIdx = staff;
                if (fullScore) cursor.rewind(0);

                var chord = null;

                while (cursor.segment && !chord) {
                    chord = findChord(cursor);
                    if (cursor.prev) {
                        cursor.prev();
                    } else {
                        break;
                    }
                }

                cursor.rewind(1); // sets voice to 0
                cursor.voice = voice;
                cursor.staffIdx = staff;
                if (fullScore) cursor.rewind(0);

                while (cursor.segment && !chord && (fullScore || cursor.tick < endTick)) {
                    cursor.next();
                    chord = findChord(cursor);
                }

                if (chord) {
                    while (cursor.segment && (fullScore || cursor.tick < endTick)) {
                        chord = findChord(cursor) || chord;
                        var el = cursor.element;
                        if (el && el.type === Element.CHORD) {
                            var existing = [];
                            for (var n in el.notes) {
                                existing.push(el.notes[n]);
                            }
                            if (!transposition) {
                                transposition = teoria.interval(
                                    teoria.note(tpcToNote(existing[0].tpc2)), 
                                    teoria.note(tpcToNote(existing[0].tpc1)));
                            }

                            var pos = (cursor.tick/480).toFixed(2)
                            console.log(pos + ': ' + chord.name, '('+transposition+' '+transposition.direction()+')');

                            var root = chord.notes()[0].transpose(transposition);
                            if (root.midi() > 64) { // e4
                                chord.transpose(teoria.Interval([-1,0]));
                            }
                            // var octave = root.octave();
                            // if (octave != 3) {
                            //     chord.transpose(teoria.Interval([3-octave,0]));
                            // }
                            var notes = chord.notes();
                            // var bass = notes()[0].transpose(teoria.Interval([-1,0]))
                            // notes.push(bass);
                            notes.forEach(function(note) {
                                var nnote = teoria.note(note.toString()).transpose(transposition);
                                var n = newElement(Element.NOTE);
                                el.add(n);
                                console.log('    ', note, '-->', nnote);
                                n.pitch = nnote.midi();
                                n.tpc1 = (7*(nnote.accidentalValue()+2) + 'fcgdaeb'.indexOf(nnote.name())) % 35 - 1;
                                n.tpc2 = (7*(note.accidentalValue()+2) + 'fcgdaeb'.indexOf(note.name())) % 35 - 1;
                                // n.tpc = (7*(nnote.accidentalValue()+2) + 'fcgdaeb'.indexOf(nnote.name())) % 35 - 1;
                            });
                            for (var n in existing) {
                                removeElement(existing[n]);
                            }
                        }
                        cursor.next();
                    }
                }
            }
        }
        curScore.endCmd();
        Qt.quit();
    }
}
