import QtQuick 2.1
import MuseScore 3.0

MuseScore {
    version:  "3.0"
    description: "Create chord colors (DRAFT)"
    menuPath: "Plugins.Chord Colors"
    requiresScore: true


    // PARAMS --> //

    readonly property var method: 'map1'; // circle / map1 / ...
    readonly property var noteIndexOffsetForCircle: 0;
    readonly property var noteOwnColor: false;

    // <-- PARAMS //


    // SETUP --> //

    readonly property var circle: 'FCGDAEB';
    readonly property var black: '#000000';
    readonly property var nc: 'NC';
    readonly property var include: ['dots', 'accidental', 'tieForward', 'stem', 'hook', 'beam', 'stemSlash'];

    readonly property var map1: {
        'C':  '#D00000',
        'Am': '#800000',

        'D5': '#508050',
        'Dm': '#007000',
        'F':  '#00B000',

        'E5': '#508080',
        'Em': '#006060',

        'G':  '#0090B0',
        'Gm': '#5000A0',
        'Bb': '#A000FF',
    }

    readonly property var palette12: [
        '#D4233B',
        '#CA1473',
        '#7C2879',
        '#494589',
        '#2070A5',
        '#0BA6A6',
        '#0FA86B',
        '#65B83F',
        '#BBD831',
        '#F0EA2F',
        '#EB9628',
        '#DC5429',
    ]

    // <-- SETUP //


    // CUSTOM FUNCS --> //

    function colorForChord(chord) {
        // console.log('chord:', chord);
        chord = chord.replace(/^[\[{(]*/, '').replace(/[\]})]*$/, '');
        if (method == 'circle') {
            var root = chord.replace(/^([a-zA-Z])(bb?|#|X])?.*/, '$1$2');
            if (!root) {
                warn('unknown chord', chord);
                return null;
            }
            var index = noteIndex(root);
            if (index == null) {
                warn('unknown chord', chord, '(root:'+root+')');
                return null;
            }
            index = wrap(index+noteIndexOffsetForCircle, 12);
            return palette12[index]

        } else if (method == 'map1') {
            var simple = chord.replace(/^([A-G])(bb?|#|X)?(m|5|dim|aug|o|O)?.*/, '$1$2$3');
            var r = map1[simple];
            if (!r) {
                warn('no color for', chord , '('+ simple +')');
            }
            return r;
        }
    }

    function findChordColor(segment, track) {
        var chord = findChord(segment, track)
        if (!chord) return null;
        if (chord.toUpperCase().replace(/\./g, '') == nc) {
            return black;
        }
        var color = colorForChord(chord);
        if (!color) {
            return black; // return null;
        }
        return color;
    }

    function colorizeNote(note, color) {
        if (noteOwnColor) {
            var _note = 'FCGDAEB'[(note.tpc+1)%7] + ['bb','b','','#','X'][Math.floor((note.tpc+1)/7)];
            var index = wrap(noteIndex(_note)+noteIndexOffsetForCircle, 12)
            color = palette12[index]
        }
        note.color = color;
        var els = note.elements;
        for (var e in els) {
            els[e].color = color;
        }
        for (var x in include) {
            x = include[x];
            if (note[x]) {
                note[x].color = color;
            }
        }
    }

    // <-- CUSTOM FUNCS //


    // FUNCS --> //

    property var warnings: []

    function warn() {
        var warning = Array.from(arguments).join(' ');
        if (warnings.indexOf(warning) == -1) {
            warnings.push(warning);
            console.warning(warning);
        }
    }

    function findChord(segment, track) {
        var anns = segment.annotations
        for (var a in anns) {
            var ann = anns[a];
            // console.log('annotation:', ann.text);
            if (ann.type == Element.HARMONY &&
                    (track == null || ann.track == track)) {
                return ann.text
            }
        }
        return null;
    }

    function colorize(element, color) {
        if (element.type === Element.CHORD) {
            var graceChords = element.graceNotes;
            for (var i = 0; i < graceChords.length; i++) {
                // iterate through all grace chords
                var graceNotes = graceChords[i].notes;
                for (var j = 0; j < graceNotes.length; j++) {
                    colorizeNote(graceNotes[j], color);
                }
            }
            var notes = element.notes;
            for (var k = 0; k < notes.length; k++) {
                colorizeNote(notes[k], color)
            }
        } else {
            element.color = color;
            for (var x in include) {
                x = include[x];
                if (element[x]) {
                    element[x].color = color;
                }
            }
        }
    }

    function wrap(x, period) {
        return x + Math.ceil(-x/period)*period;
    }

    function noteIndex(note) {
        var index = circle.indexOf(note[0].toUpperCase());
        if (index == -1) {
            return null;
        }
        var accidental = note.slice(1);
        if (accidental == 'b') {
            return index - 7;
        } else if (accidental == 'bb') {
            return index - 14;
        } else if (accidental == '#') {
            return index + 7;
        } else if (accidental == 'X') {
            return index + 14;
        } else if (accidental != '') {
            return null;
        } else {
            return index;
        }
    }

    // <-- FUNCS //


    function elt(el) {
        var elt = new Object();
        elt[Element.INVALID] = 'INVALID'
        elt[Element.BRACKET_ITEM] = 'BRACKET_ITEM'
        elt[Element.PART] = 'PART'
        elt[Element.STAFF] = 'STAFF'
        elt[Element.SCORE] = 'SCORE'
        elt[Element.SYMBOL] = 'SYMBOL'
        elt[Element.TEXT] = 'TEXT'
        elt[Element.MEASURE_NUMBER] = 'MEASURE_NUMBER'
        elt[Element.INSTRUMENT_NAME] = 'INSTRUMENT_NAME'
        elt[Element.SLUR_SEGMENT] = 'SLUR_SEGMENT'
        elt[Element.TIE_SEGMENT] = 'TIE_SEGMENT'
        elt[Element.BAR_LINE] = 'BAR_LINE'
        elt[Element.STAFF_LINES] = 'STAFF_LINES'
        elt[Element.SYSTEM_DIVIDER] = 'SYSTEM_DIVIDER'
        elt[Element.STEM_SLASH] = 'STEM_SLASH'
        elt[Element.ARPEGGIO] = 'ARPEGGIO'
        elt[Element.ACCIDENTAL] = 'ACCIDENTAL'
        elt[Element.LEDGER_LINE] = 'LEDGER_LINE'
        elt[Element.STEM] = 'STEM'
        elt[Element.NOTE] = 'NOTE'
        elt[Element.CLEF] = 'CLEF'
        elt[Element.KEYSIG] = 'KEYSIG'
        elt[Element.AMBITUS] = 'AMBITUS'
        elt[Element.TIMESIG] = 'TIMESIG'
        elt[Element.REST] = 'REST'
        elt[Element.BREATH] = 'BREATH'
        elt[Element.REPEAT_MEASURE] = 'REPEAT_MEASURE'
        elt[Element.TIE] = 'TIE'
        elt[Element.ARTICULATION] = 'ARTICULATION'
        elt[Element.FERMATA] = 'FERMATA'
        elt[Element.CHORDLINE] = 'CHORDLINE'
        elt[Element.DYNAMIC] = 'DYNAMIC'
        elt[Element.BEAM] = 'BEAM'
        elt[Element.HOOK] = 'HOOK'
        elt[Element.LYRICS] = 'LYRICS'
        elt[Element.FIGURED_BASS] = 'FIGURED_BASS'
        elt[Element.MARKER] = 'MARKER'
        elt[Element.JUMP] = 'JUMP'
        elt[Element.FINGERING] = 'FINGERING'
        elt[Element.TUPLET] = 'TUPLET'
        elt[Element.TEMPO_TEXT] = 'TEMPO_TEXT'
        elt[Element.STAFF_TEXT] = 'STAFF_TEXT'
        elt[Element.SYSTEM_TEXT] = 'SYSTEM_TEXT'
        elt[Element.REHEARSAL_MARK] = 'REHEARSAL_MARK'
        elt[Element.INSTRUMENT_CHANGE] = 'INSTRUMENT_CHANGE'
        elt[Element.STAFFTYPE_CHANGE] = 'STAFFTYPE_CHANGE'
        elt[Element.HARMONY] = 'HARMONY'
        elt[Element.FRET_DIAGRAM] = 'FRET_DIAGRAM'
        elt[Element.BEND] = 'BEND'
        elt[Element.TREMOLOBAR] = 'TREMOLOBAR'
        elt[Element.VOLTA] = 'VOLTA'
        elt[Element.HAIRPIN_SEGMENT] = 'HAIRPIN_SEGMENT'
        elt[Element.OTTAVA_SEGMENT] = 'OTTAVA_SEGMENT'
        elt[Element.TRILL_SEGMENT] = 'TRILL_SEGMENT'
        elt[Element.LET_RING_SEGMENT] = 'LET_RING_SEGMENT'
        elt[Element.VIBRATO_SEGMENT] = 'VIBRATO_SEGMENT'
        elt[Element.PALM_MUTE_SEGMENT] = 'PALM_MUTE_SEGMENT'
        elt[Element.TEXTLINE_SEGMENT] = 'TEXTLINE_SEGMENT'
        elt[Element.VOLTA_SEGMENT] = 'VOLTA_SEGMENT'
        elt[Element.PEDAL_SEGMENT] = 'PEDAL_SEGMENT'
        elt[Element.LYRICSLINE_SEGMENT] = 'LYRICSLINE_SEGMENT'
        elt[Element.GLISSANDO_SEGMENT] = 'GLISSANDO_SEGMENT'
        elt[Element.LAYOUT_BREAK] = 'LAYOUT_BREAK'
        elt[Element.SPACER] = 'SPACER'
        elt[Element.STAFF_STATE] = 'STAFF_STATE'
        elt[Element.NOTEHEAD] = 'NOTEHEAD'
        elt[Element.NOTEDOT] = 'NOTEDOT'
        elt[Element.TREMOLO] = 'TREMOLO'
        elt[Element.IMAGE] = 'IMAGE'
        elt[Element.MEASURE] = 'MEASURE'
        elt[Element.SELECTION] = 'SELECTION'
        elt[Element.LASSO] = 'LASSO'
        elt[Element.SHADOW_NOTE] = 'SHADOW_NOTE'
        elt[Element.TAB_DURATION_SYMBOL] = 'TAB_DURATION_SYMBOL'
        elt[Element.FSYMBOL] = 'FSYMBOL'
        elt[Element.PAGE] = 'PAGE'
        elt[Element.HAIRPIN] = 'HAIRPIN'
        elt[Element.OTTAVA] = 'OTTAVA'
        elt[Element.PEDAL] = 'PEDAL'
        elt[Element.TRILL] = 'TRILL'
        elt[Element.LET_RING] = 'LET_RING'
        elt[Element.VIBRATO] = 'VIBRATO'
        elt[Element.PALM_MUTE] = 'PALM_MUTE'
        elt[Element.TEXTLINE] = 'TEXTLINE'
        elt[Element.TEXTLINE_BASE] = 'TEXTLINE_BASE'
        elt[Element.NOTELINE] = 'NOTELINE'
        elt[Element.LYRICSLINE] = 'LYRICSLINE'
        elt[Element.GLISSANDO] = 'GLISSANDO'
        elt[Element.BRACKET] = 'BRACKET'
        elt[Element.SEGMENT] = 'SEGMENT'
        elt[Element.SYSTEM] = 'SYSTEM'
        elt[Element.COMPOUND] = 'COMPOUND'
        elt[Element.CHORD] = 'CHORD'
        elt[Element.SLUR] = 'SLUR'
        elt[Element.ELEMENT] = 'ELEMENT'
        elt[Element.ELEMENT_LIST] = 'ELEMENT_LIST'
        elt[Element.STAFF_LIST] = 'STAFF_LIST'
        elt[Element.MEASURE_LIST] = 'MEASURE_LIST'
        elt[Element.HBOX] = 'HBOX'
        elt[Element.VBOX] = 'VBOX'
        elt[Element.TBOX] = 'TBOX'
        elt[Element.FBOX] = 'FBOX'
        elt[Element.ICON] = 'ICON'
        elt[Element.OSSIA] = 'OSSIA'
        elt[Element.BAGPIPE_EMBELLISHMENT] = 'BAGPIPE_EMBELLISHMENT'
        elt[Element.STICKING] = 'STICKING'
        elt[Element.MAXTYPE] = 'MAXTYPE'
        return elt[el.type];
    }



    onRun: {
        curScore.startCmd();

        var cursor = curScore.newCursor();
        cursor.rewind(1);
        var startStaff;
        var endStaff;
        var endTick;
        var fullScore = false;
        if (!cursor.segment) { // no selection
          fullScore = true;
          startStaff = 0; // start with 1st staff
          endStaff = curScore.nstaves - 1; // and end with last
        } else {
            startStaff = cursor.staffIdx;
            cursor.rewind(2);
            if (cursor.tick === 0) {
                // this happens when the selection includes
                // the last measure of the score.
                // rewind(2) goes behind the last segment (where
                // there's none) and sets tick=0
                endTick = curScore.lastSegment.tick + 1;
            } else {
                endTick = cursor.tick;
            }
            endStaff = cursor.staffIdx;
        }

        if (fullScore) {
            cursor.rewind(0);
            var track = null;
            console.log('Applying to full score');
        } else {
            cursor.rewind(1);
            var track = cursor.track;
            console.log('Applying to selection only');
        }

        var color = null;

        var segment = cursor.segment;

        // search back //
        while (segment && !color) {
            color = findChordColor(segment, track);
            segment = segment.prev;
        }

        segment = cursor.segment;

        // search forward //
        while (segment && !color && (fullScore || segment.tick < endTick)) {
            color = findChordColor(segment, track);
            segment = segment.next;
        }

        segment = cursor.segment;

        if (color) {
            while (segment && (fullScore || segment.tick < endTick)) {
                color = findChordColor(segment, track) || color;
                for (var staff = startStaff; staff <= endStaff; staff++) {
                    for (var voice = 0; voice < 4; voice++) {
                        var _track = staff*4+voice;
                        var element = segment.elementAt(_track);
                        if (element && 
                                (element.type == Element.CHORD 
                                || element.type == Element.REST)) {
                            // console.log('found element', elt(element), 'at', segment.tick/480);
                            colorize(element, color);
                        }
                    }
                }
                var anns = segment.annotations
                for (var a in anns) {
                    var ann = anns[a];
                    if (ann.type == Element.HARMONY) {
                        ann.color = color;
                    }
                }
                segment = segment.next;
            }
        } else {
            warn('did not find any chords');
        }
        curScore.endCmd();
        Qt.quit();
    }
}
